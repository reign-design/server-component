const request = require('supertest');
var should = require('chai').should();
const app = require('../app');
const postFunctions = require('../functions/posts');
var result;
beforeEach( async ()=>{
    result = await postFunctions.getPostFromHackerNews();
});

describe('- Posts Functions',async()=> {
    /* describe('1) Get posts from Hackernews',()=>{
        it('When dont retrive any post', async (done)=> {
            const result = await postFunctions.getPostFromHackerNews();
            result.should.be.an('object');
            result.should.have.property('error');
            result.error.should.have.an('boolean');
            result.error.should.be.equals(false);
            result.should.have.property('message');
            result.message.should.have.an('string');
            result.should.have.property('posts');
            result.posts.should.have.an('array');
            result.posts.length.should.be.equals(0);
            done();
        });
    }); */
    describe('3) Get posts from Hackernews',()=>{
        it('When retrieve posts successfully', async (done)=> {
            result.should.be.an('object');
            result.should.have.property('error');
            result.error.should.have.an('boolean');
            result.error.should.be.equals(false);
            result.should.have.property('message');
            result.message.should.have.an('string');
            result.should.have.property('posts');
            result.posts.should.have.an('array');
            result.posts.length.should.be.at.least(1);
            done();
        });
    });
});
