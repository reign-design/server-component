const request = require('supertest');
const app = require('../app');

describe('App', function() {
  it('has the default page', function(done) {
    request(app)
      .get('/')
      .expect('Welcome, this is the Hilder Postulation API!', done);
  });
}); 

describe('Posts', function() {
  it('has the default page', function(done) {
    request(app)
      .get('/posts')
      .expect('Post endpoint', done);
  });
});
