
const axios = require('axios');
const mongoose = require('mongoose')
const postSchema = require('../database/schemas/post');

const Post = mongoose.model('Post', postSchema);

const postsfunctions = {
    getPostFromHackerNews: async ()=>{
        try {
            const {data} = await axios.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs');
            return (data) ? {
                error:false,
                message: 'The posts from Hacker news had been requested successfully',
                posts: data.hits
            } : {
                error:false,
                message: 'Un mensaje',
                posts: []
            } ;
        } catch (error) {
            return {
                error:true,
                message: 'An unexpected error trying to get post from Hacker News',
                posts: []
            };
        }
    },
    getPosts: async ()=>{
        try {

            Post.find((err,posts)=>{
                console.log("Post",posts);
            });
        } catch (error) {
            console.log("Error",error)
        }
    }
    
}


module.exports = postsfunctions;