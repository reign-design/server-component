const mongoose = require('mongoose');

const postSchema = new mongoose.Schema({
    author:String,
    date: Date,
    title:String,
    url:String,
    enabled:Boolean
});


module.exports = postSchema;