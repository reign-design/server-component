
const mongoose = require('mongoose');
const databasehost = 'mongodb://mongo/mongo';

const database = {
    starConnection: ()=>{
        mongoose.connect(databasehost,{ useNewUrlParser: true,useUnifiedTopology: true });
    }
}

module.exports = database;