var express = require('express');
var router = express.Router();
var postFunctions =  require('../functions/posts');


router.get('/', async (req, res, next) => {
  postFunctions.getPosts();
  res.send('Post endpoint');
});

module.exports = router;
