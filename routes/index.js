var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.send('Welcome, this is the Hilder Postulation API!');
});

module.exports = router;
